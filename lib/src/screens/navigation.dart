import 'package:Movies/src/screens/movieSearch.dart';
import 'package:Movies/src/screens/user.dart';
import 'package:flutter/material.dart';
import 'package:Movies/src/screens/home.dart';
import 'package:Movies/src/screens/notifications.dart';

class Navigation extends StatefulWidget {
  static final String routeName = '/';

  @override
  _NavigationState createState() => _NavigationState();
}

class _NavigationState extends State<Navigation> {
  int _currentIndex = 1;
  final tabs = [Home(), MovieSearch(), Notifications(), User()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: tabs[_currentIndex],
      bottomNavigationBar: _menuBottom(),
    );
  }

  Widget _menuBottom() {
    return BottomNavigationBar(
      currentIndex: _currentIndex,
      iconSize: 35,
      selectedLabelStyle: TextStyle(fontSize: 11, color: Colors.white),
      unselectedIconTheme: IconThemeData(size: 25),
      type: BottomNavigationBarType.shifting,
      onTap: (index) {
        setState(() {
          _currentIndex = index;
        });
        print(index);
      },
      items: [
        BottomNavigationBarItem(
            icon: Icon(
              Icons.home_rounded,
              color: Colors.white,
            ),
            label: 'Home',
            backgroundColor: Colors.black),
        BottomNavigationBarItem(
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            label: 'Search',
            backgroundColor: Colors.black),
        BottomNavigationBarItem(
            icon: Icon(
              Icons.notifications_rounded,
              color: Colors.white,
            ),
            label: 'Alerts',
            backgroundColor: Colors.black),
        BottomNavigationBarItem(
            icon: Icon(
              Icons.person_sharp,
              color: Colors.white,
            ),
            label: 'Profile',
            backgroundColor: Colors.black)
      ],
    );
  }
}
