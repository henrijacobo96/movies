import 'package:Movies/src/models/movie.dart';
import 'package:Movies/src/resources/movieProvider.dart';
import 'package:Movies/src/screens/movieDetail.dart';
import 'package:Movies/src/utils/loading.dart';
import 'package:Movies/src/utils/movieList.dart';
import 'package:Movies/src/utils/noData.dart';
import 'package:Movies/src/utils/sizeHelper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

class MovieSearch extends StatefulWidget {
  static final String routeName = 'movieSearch';

  @override
  _MovieSearchState createState() => _MovieSearchState();
}

class _MovieSearchState extends State<MovieSearch> {
  final searchTextController = new TextEditingController();
  String searchMovie = "";
  int page = 1;

  @override
  void dispose() {
    searchTextController.dispose();
    super.dispose();
  }

  void movieClick(Movie item) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => MovieDetail(
                  movieName: item.title,
                  imdbId: item.imdbID,
                )));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Buscar Películas'),
        centerTitle: true,
      ),
      body: Column(
        children: <Widget>[
          Container(
            child: Row(
              children: <Widget>[
                Flexible(
                    child: TextField(
                  maxLines: 1,
                  controller: searchTextController,
                  decoration: InputDecoration(
                    hintText: 'Buscar por nombre',
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(25.0))),
                  ),
                  onChanged: (value) {
                    setState(() {
                      searchMovie = value;
                    });
                  },
                )),
              ],
            ),
            padding: EdgeInsets.all(10.0),
          ),
          if (searchMovie.length > 0)
            Column(children: <Widget>[separatorType(), paginator()])
        ],
      ),
    );
  }

  Widget pageTor() {
    return Material(
      child: Center(
        child: Ink(
          decoration: ShapeDecoration(
            color: Colors.grey[800],
            shape: CircleBorder(),
          ),
          child: IconButton(
              icon: Icon(
                Icons.navigate_before_sharp,
                size: 30,
              ),
              onPressed: () {
                if (page == 1) {
                  setState(() {
                    page = 1;
                  });
                } else {
                  setState(() {
                    page = page - 1;
                  });
                }
              }),
        ),
      ),
    );
  }

  Widget paginator() {
    return Container(
      width: displayWidth(context),
      alignment: Alignment.topCenter,
      height: displayHeight(context) * 0.05,
      child: ButtonTheme(
        buttonColor: Colors.white38,
        child: ButtonBar(
          //buttonHeight: displayHeight(context) * 0.3,
          alignment: MainAxisAlignment.center,
          children: <Widget>[
            FlatButton.icon(
                label: Text(''),
                icon: Icon(
                  Icons.navigate_before_sharp,
                  size: 30,
                ),
                onPressed: () {
                  if (page == 1) {
                    setState(() {
                      page = 1;
                    });
                  } else {
                    setState(() {
                      page = page - 1;
                    });
                  }
                }),
            /*Container(
              alignment: Alignment.bottomLeft,
              child: Text('$page'),
            ),*/
            FlatButton.icon(
                label: Text(''),
                icon: Icon(
                  Icons.navigate_next_sharp,
                  size: 30,
                ),
                onPressed: () {
                  setState(() {
                    page = page + 1;
                  });
                }),
          ],
        ),
      ),
    );
    /*FloatingActionButton(
      onPressed: () {
        setState(() {
          page = page + 1;
        });
      },
      child: Icon(
        Icons.navigate_next_rounded,
        size: 40,
      ),
      mini: true,
    );*/
  }

  Widget separatorType() {
    return DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            child: TabBar(
              tabs: [
                Tab(
                  text: 'Películas',
                ),
                Tab(
                  text: 'Series',
                )
              ],
            ),
          ),
          Container(
            height: displayHeight(context) * 0.60,
            decoration: BoxDecoration(
                border:
                    Border(top: BorderSide(color: Colors.grey, width: 0.5))),
            child: TabBarView(
              children: <Widget>[
                FutureBuilder<List<Movie>>(
                  future: searchMovies(searchMovie, page, 'movie'),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Expanded(
                        child: MovieList(
                          movies: snapshot.data,
                          itemClick: this.movieClick,
                        ),
                      );
                    } else if (snapshot.hasError) {
                      return Text('To many, keep waiting');
                    } else if (snapshot.error == "Too many results.") {
                      return Text('To many, keep waiting');
                    }
                    return Loading();
                  },
                ),
                FutureBuilder<List<Movie>>(
                  future: searchMovies(searchMovie, page, 'series'),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Expanded(
                        child: MovieList(
                          movies: snapshot.data,
                          itemClick: this.movieClick,
                        ),
                      );
                    } else if (snapshot.hasError) {
                      return Text('To many, keep waiting');
                    } else if (snapshot.error == "Too many results.") {
                      return Text('To many, keep waiting');
                    }
                    return Loading();
                  },
                )
                /*Container(
                    child:
                        MovieList(movies: movies, itemClick: this.movieClick)),
                Container(
                    child:
                        MovieList(movies: series, itemClick: this.movieClick))*/
              ],
            ),
          )
        ],
      ),
    );
  }
}
