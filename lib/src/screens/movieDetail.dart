import 'package:Movies/src/utils/loading.dart';
import 'package:Movies/src/utils/noData.dart';
import 'package:flutter/material.dart';
import 'package:Movies/src/resources/movieProvider.dart';
import 'package:Movies/src/models/movieDetails.dart';
import 'package:Movies/src/utils/paddedText.dart';
import 'package:percent_indicator/percent_indicator.dart';

class MovieDetail extends StatelessWidget {
  final String movieName;
  final String imdbId;

  MovieDetail({this.movieName, this.imdbId});

  @override
  Widget build(BuildContext context) {
    double score;
    double imdb;
    return Scaffold(
      appBar: AppBar(
        title: Text('Details'),
        centerTitle: true,
      ),
      body: FutureBuilder<MovieDetails>(
        future: getMovie(this.imdbId),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.metaScore == 'N/A') {
              score = 0;
            } else {
              score = double.parse(snapshot.data.metaScore);
            }
            if (snapshot.data.imdbRating == 'N/A') {
              imdb = 0;
            } else {
              imdb = double.parse(snapshot.data.imdbRating);
            }
            return Container(
              padding: EdgeInsets.all(20),
              //I'm not so good at UI design
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                        alignment: Alignment.center,
                        child: Image.network(
                          snapshot.data.poster,
                          width: 100,
                          height: 120,
                        ),
                      ),
                    ],
                  ),
                  PaddedText("Title : " + this.movieName),
                  Container(
                    height: 10,
                  ),
                  Text(
                    'Plot: ' + snapshot.data.plot,
                    textAlign: TextAlign.justify,
                    style: TextStyle(fontSize: 19),
                  ),
                  PaddedText("Year : " + snapshot.data.year),
                  PaddedText("Genre : " + snapshot.data.genre),
                  PaddedText("Directed by : " + snapshot.data.director),
                  PaddedText("Runtime : " + snapshot.data.runtime),
                  Container(
                    height: 10,
                  ),
                  Container(
                    child: Row(
                      children: <Widget>[
                        CircularPercentIndicator(
                          radius: 40,
                          lineWidth: 5,
                          percent: score / 100,
                          center: Text(snapshot.data.metaScore,
                              style: TextStyle(fontSize: 18)),
                          progressColor: Colors.green[700],
                          footer: Text(
                            'Score',
                            style: TextStyle(fontSize: 20),
                          ),
                        ),
                        Container(
                          width: 10,
                        ),
                        CircularPercentIndicator(
                          radius: 40,
                          lineWidth: 5,
                          percent: imdb / 10,
                          center: Text(snapshot.data.imdbRating,
                              style: TextStyle(fontSize: 18)),
                          progressColor: Colors.green[700],
                          footer: Text('IMDB', style: TextStyle(fontSize: 20)),
                        )
                      ],
                    ),
                  ),
                  Container(
                    height: 10,
                  ),
                  PaddedText("Rated : " + snapshot.data.rating),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            return NoData();
          } else {
            return Loading();
          }
        },
      ),
    );
  }
}
