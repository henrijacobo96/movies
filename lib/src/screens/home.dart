import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  static final String routeName = 'homePage';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // backgroundColor: Colors.grey[900],
        title: Text('Home'),
        centerTitle: true,
      ),
    );
  }
}
