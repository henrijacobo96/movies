import 'package:flutter/material.dart';
import 'package:Movies/src/models/movie.dart' as models;

class MovieItem extends StatelessWidget {
  final models.Movie movie;

  MovieItem({this.movie});

  @override
  Widget build(BuildContext context) {
    return movies();
  }

  //testing with cards
  Widget movies() {
    return Card(
      elevation: 10.0,
      color: Colors.grey[900],
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          ListTile(
            leading: Column(
              children: <Widget>[
                if (this.movie.poster != "N/A")
                  Image.network(
                    this.movie.poster,
                    height: 50,
                    width: 30,
                    fit: BoxFit.cover,
                  )
              ],
            ),
            title: Text(
              this.movie.title,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
            ),
            subtitle: Text(this.movie.type + ' - ' + this.movie.year),
          )
        ],
      ),
    );
  }
}
