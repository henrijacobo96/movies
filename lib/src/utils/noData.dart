import 'package:flutter/material.dart';

class NoData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: <Widget>[
        Container(
          height: 60,
        ),
        Image(
          image:
              NetworkImage('http://cdn.onlinewebfonts.com/svg/img_56611.png'),
        ),
        Container(
          height: 20,
        ),
        Text(
          'Algo salió mal',
          style: TextStyle(
            fontSize: 25,
          ),
        ),
        Text(
          'Y no sabemos qué',
          style: TextStyle(
            fontSize: 25,
          ),
        )
      ],
    ));
  }
}
