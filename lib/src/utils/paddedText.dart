import 'package:flutter/material.dart';

class PaddedText extends StatelessWidget {
  final String text;

  PaddedText(this.text);

  @override
  Widget build(BuildContext contex) {
    return Padding(
        child: Text(
          this.text,
          style: TextStyle(fontSize: 19),
        ),
        padding: EdgeInsets.only(top: 5, bottom: 5));
  }
}
