import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:Movies/src/models/movieDetails.dart';
import 'package:Movies/src/models/movie.dart';

const apiKey = "da622696";
const apiURL = "http://www.omdbapi.com/?apikey=";

Future<List<Movie>> searchMovies(query, int page, type) async {
  //get results based on query (maybe)
  final response =
      await http.get('$apiURL$apiKey&s=$query&page=$page&type=$type');

  if (response.statusCode == 200) {
    Map data = json.decode(response.body);

    if (data['Response'] == "True") {
      var list = (data['Search'] as List)
          .map((item) => new Movie.fromJson(item))
          .toList();
      return list;
    } else {
      throw Exception(data['Error']);
    }
  } else {
    throw Exception('Something went wrong !');
  }
}

//for movie details
Future<MovieDetails> getMovie(movieID) async {
  //same as above
  final response = await http.get('$apiURL$apiKey&i=$movieID');

  if (response.statusCode == 200) {
    Map data = json.decode(response.body);

    if (data['Response'] == "True") {
      return MovieDetails.fromJson(data);
    } /*else if(data['Response'] == "False" && data["Error"] == "Too many results."){
      
    }*/
    else {
      throw Exception(data['Error']);
    }
  } else {
    throw Exception('Algo salió mal :c');
  }
}
